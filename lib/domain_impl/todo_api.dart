import 'package:powerfull_mixin_example/domain/model/todo_model.dart';
import 'package:powerfull_mixin_example/domain/todo_api.dart';

class TodoApiImpl extends TodoApi {
  @override
  Future<void> deleteById(String id) async {}

  @override
  Future<TodoModel> getById(String id) async =>
      TodoModel(id: id, parentId: null, content: "content");

  @override
  Future<List<TodoModel>> getByParentId(String? id) async => [
        TodoModel(
          id: DateTime.now().toString(),
          parentId: id,
          content: "content",
        )
      ];

  @override
  Future<void> save(TodoModel model) async {}
}
