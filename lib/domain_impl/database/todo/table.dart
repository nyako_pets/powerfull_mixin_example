import 'package:dependencies/export_impl.dart';
import 'package:moor/moor.dart';

import 'converter.dart';

class TodoTable extends Table
    with WithIdColumn<String>, WithParentIdColumn<String?> {
  TextColumn get id => text()();

  TextColumn get parentId => text().nullable()();

  TextColumn get value => text().map(const ContentConverter())();

  @override
  Set<Column>? get primaryKey => {id};
}
