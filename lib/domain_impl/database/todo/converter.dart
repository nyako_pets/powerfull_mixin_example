import 'package:dependencies/export_impl.dart';
import 'package:powerfull_mixin_example/domain/model/todo_model.dart';

class ContentConverter extends JsonTypeConverter<TodoModel> {
  const ContentConverter();

  @override
  Map<String, dynamic> toJson(TodoModel fromDb) => fromDb.toJson();

  @override
  TodoModel fromJson(Map<String, dynamic> map) => TodoModel.fromJson(map);
}
