import 'package:dependencies/export_impl.dart';
import 'package:powerfull_mixin_example/domain/model/todo_model.dart';
import 'package:powerfull_mixin_example/domain/todo_dao.dart';

import '../database.dart';

class TodoDaoImpl extends TodoDao
    with
        WithTable<$TodoTableTable, TodoTableData>,
        WithMapper<TodoTableData, TodoModel>,
        DaoMixin,
        DaoMixinById,
        DaoMixinByParentId {
  final DatabaseAccessor<Database> accessor;

  TodoDaoImpl(Database attachedDatabase)
      : accessor = DatabaseAccessorImpl(attachedDatabase);

  @override
  TodoModel mapFromDb(TodoTableData model) => model.value;

  @override
  TodoTableData mapToDb(TodoModel model) => TodoTableData(
        value: model,
        id: model.id,
        parentId: model.parentId,
      );

  @override
  TableInfo<$TodoTableTable, TodoTableData> get table =>
      accessor.attachedDatabase.todoTable;
}
