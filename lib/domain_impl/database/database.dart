import 'package:dependencies/export_impl.dart';
import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'package:powerfull_mixin_example/domain/model/todo_model.dart';

import 'todo/converter.dart';
import 'todo/table.dart';

part 'database.g.dart';

@UseMoor(
  tables: [TodoTable],
)
class Database extends _$Database {
  Database(String name) : super(DatabaseUtils.openConnection(name));

  @override
  int get schemaVersion => 1;
}
