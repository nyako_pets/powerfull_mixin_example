// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class TodoTableData extends DataClass implements Insertable<TodoTableData> {
  final String id;
  final String? parentId;
  final TodoModel value;
  TodoTableData({required this.id, this.parentId, required this.value});
  factory TodoTableData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return TodoTableData(
      id: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      parentId: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}parent_id']),
      value: $TodoTableTable.$converter0.mapToDart(const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}value']))!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<String>(id);
    if (!nullToAbsent || parentId != null) {
      map['parent_id'] = Variable<String?>(parentId);
    }
    {
      final converter = $TodoTableTable.$converter0;
      map['value'] = Variable<String>(converter.mapToSql(value)!);
    }
    return map;
  }

  TodoTableCompanion toCompanion(bool nullToAbsent) {
    return TodoTableCompanion(
      id: Value(id),
      parentId: parentId == null && nullToAbsent
          ? const Value.absent()
          : Value(parentId),
      value: Value(value),
    );
  }

  factory TodoTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return TodoTableData(
      id: serializer.fromJson<String>(json['id']),
      parentId: serializer.fromJson<String?>(json['parentId']),
      value: serializer.fromJson<TodoModel>(json['value']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'parentId': serializer.toJson<String?>(parentId),
      'value': serializer.toJson<TodoModel>(value),
    };
  }

  TodoTableData copyWith({String? id, String? parentId, TodoModel? value}) =>
      TodoTableData(
        id: id ?? this.id,
        parentId: parentId ?? this.parentId,
        value: value ?? this.value,
      );
  @override
  String toString() {
    return (StringBuffer('TodoTableData(')
          ..write('id: $id, ')
          ..write('parentId: $parentId, ')
          ..write('value: $value')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(parentId.hashCode, value.hashCode)));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is TodoTableData &&
          other.id == this.id &&
          other.parentId == this.parentId &&
          other.value == this.value);
}

class TodoTableCompanion extends UpdateCompanion<TodoTableData> {
  final Value<String> id;
  final Value<String?> parentId;
  final Value<TodoModel> value;
  const TodoTableCompanion({
    this.id = const Value.absent(),
    this.parentId = const Value.absent(),
    this.value = const Value.absent(),
  });
  TodoTableCompanion.insert({
    required String id,
    this.parentId = const Value.absent(),
    required TodoModel value,
  })  : id = Value(id),
        value = Value(value);
  static Insertable<TodoTableData> custom({
    Expression<String>? id,
    Expression<String?>? parentId,
    Expression<TodoModel>? value,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (parentId != null) 'parent_id': parentId,
      if (value != null) 'value': value,
    });
  }

  TodoTableCompanion copyWith(
      {Value<String>? id, Value<String?>? parentId, Value<TodoModel>? value}) {
    return TodoTableCompanion(
      id: id ?? this.id,
      parentId: parentId ?? this.parentId,
      value: value ?? this.value,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (parentId.present) {
      map['parent_id'] = Variable<String?>(parentId.value);
    }
    if (value.present) {
      final converter = $TodoTableTable.$converter0;
      map['value'] = Variable<String>(converter.mapToSql(value.value)!);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TodoTableCompanion(')
          ..write('id: $id, ')
          ..write('parentId: $parentId, ')
          ..write('value: $value')
          ..write(')'))
        .toString();
  }
}

class $TodoTableTable extends TodoTable
    with TableInfo<$TodoTableTable, TodoTableData> {
  final GeneratedDatabase _db;
  final String? _alias;
  $TodoTableTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  late final GeneratedColumn<String?> id = GeneratedColumn<String?>(
      'id', aliasedName, false,
      typeName: 'TEXT', requiredDuringInsert: true);
  final VerificationMeta _parentIdMeta = const VerificationMeta('parentId');
  late final GeneratedColumn<String?> parentId = GeneratedColumn<String?>(
      'parent_id', aliasedName, true,
      typeName: 'TEXT', requiredDuringInsert: false);
  final VerificationMeta _valueMeta = const VerificationMeta('value');
  late final GeneratedColumnWithTypeConverter<TodoModel, String?> value =
      GeneratedColumn<String?>('value', aliasedName, false,
              typeName: 'TEXT', requiredDuringInsert: true)
          .withConverter<TodoModel>($TodoTableTable.$converter0);
  @override
  List<GeneratedColumn> get $columns => [id, parentId, value];
  @override
  String get aliasedName => _alias ?? 'todo_table';
  @override
  String get actualTableName => 'todo_table';
  @override
  VerificationContext validateIntegrity(Insertable<TodoTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('parent_id')) {
      context.handle(_parentIdMeta,
          parentId.isAcceptableOrUnknown(data['parent_id']!, _parentIdMeta));
    }
    context.handle(_valueMeta, const VerificationResult.success());
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  TodoTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    return TodoTableData.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $TodoTableTable createAlias(String alias) {
    return $TodoTableTable(_db, alias);
  }

  static TypeConverter<TodoModel, String> $converter0 =
      const ContentConverter();
}

abstract class _$Database extends GeneratedDatabase {
  _$Database(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  late final $TodoTableTable todoTable = $TodoTableTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [todoTable];
}
