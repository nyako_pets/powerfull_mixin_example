import 'package:powerfull_mixin_example/domain/todo_api.dart';
import 'package:powerfull_mixin_example/domain/todo_dao.dart';
import 'package:powerfull_mixin_example/provider/functions/from_api_or_dao.dart';

class TodoProvider {
  final TodoDao _dao;
  final TodoApi _api;

  TodoProvider(this._dao, this._api);

  late final getById = FromDaoOrApi.byId(api: _api, dao: _dao);

  late final getByParentId = FromDaoOrApi.byParentId(api: _api, dao: _dao);
}
