import 'package:dependencies/export.dart';

class FromDaoOrApi<ID, M> {
  final Future<M> Function(ID id) _getFromApi;
  final Stream<M> Function(ID id) _listenFromApi;

  FromDaoOrApi._(this._getFromApi, this._listenFromApi);

  static FromDaoOrApi<ID, M> byId<ID, M>({
    required GetById<ID, M> api,
    required ListenById<ID, M> dao,
  }) =>
      FromDaoOrApi._(api.getById, dao.listenById);

  static FromDaoOrApi<ID, List<M>> byParentId<ID, M>({
    required GetByParentId<ID, M> api,
    required ListenByParentId<ID, M> dao,
  }) =>
      FromDaoOrApi._(api.getByParentId, dao.listenByParentId);

  Stream<M> call(ID id) => CombineLatestStream(
        [
          _listenFromApi(id).cast<M?>().startWith(null),
          _getFromApi(id).asStream().cast<M?>().startWith(null),
        ],
        (values) {
          final cached = values[0] as M?;
          final network = values[1] as M?;
          return network ?? cached;
        },
      ).notNull();
}
