import 'package:dependencies/export.dart';

import 'model/todo_model.dart';

abstract class TodoDao extends DataApi<TodoModel>
    with
        ById<String>,
        ByParentId<String?>,
        ListenById,
        ListenByParentId,
        Save,
        DeleteById {}
