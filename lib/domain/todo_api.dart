import 'package:dependencies/export.dart';

import 'model/todo_model.dart';

abstract class TodoApi extends DataApi<TodoModel>
    with
        ById<String>,
        ByParentId<String?>,
        GetById,
        GetByParentId,
        Save,
        DeleteById {}
